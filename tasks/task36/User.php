<?php

declare(strict_types=1);

namespace tasks\task36;

/**
 * Class User
 * @package tasks\task36
 */
class User
{
    /** @var string $name */
    private string $name;

    /** @var string $surname */
    private string $surname;

    /** @var string $patronymic */
    private string $patronymic;

    public function __construct(string $name, string $surname, string $patronymic)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->patronymic = $patronymic;
    }

    public function __toString()
    {
        return $this->surname.$this->name.' '.$this->patronymic;
    }
}
