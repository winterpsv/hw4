<?php

declare(strict_types=1);

namespace tasks\task38;

/**
 * Class FirstException
 * @package tasks\task38
 */
class FirstException extends \Exception
{
}
