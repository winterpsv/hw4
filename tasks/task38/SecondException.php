<?php

declare(strict_types=1);

namespace tasks\task38;

use tasks\task38\ThirdException as Third;

/**
 * Class SecondException
 * @package tasks\task38
 */
class SecondException extends Third
{
}
