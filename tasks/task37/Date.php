<?php

declare(strict_types=1);

namespace tasks\task37;

/**
 * Class Date
 * @package tasks\task37
 */
class Date
{
    /** @var int $year */
    public int $year;

    /** @var int $month */
    public int $month;

    /** @var int $day */
    public int $day;

    public function __get($name)
    {
        if ('weekDay' === $name) {
            return date("N D", strtotime("$this->year-$this->month-$this->day"));
        }
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __call($methodName, $args)
    {
        if('method2' === $methodName) {
            echo 'Метод method2 будет реализован в следующих версиях приложения<br>';
            return;
        }
        return $this->$methodName();
    }

    private function method1()
    {
        echo 'Вызван приватный метод method1<br>';
    }
}
