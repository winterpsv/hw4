<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

use tasks\task36\User as Task36;
use tasks\task37\Date as Task37;
use tasks\task38\FirstException;
use tasks\task38\SecondException;
use tasks\task38\ThirdException;
use tasks\task41\ErrHandler as Task41;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

/*
 * Task 36
 * Задача 36.1: Сделайте класс User,
 * в котором будут следующие приватные свойства - name (имя),
 * surname (фамилия), patronymic (отчество).
 * Задача 36.2: Сделайте так, чтобы при выводе объекта 	через echo на экран выводилось
 * ФИО пользователя (фамилия, имя, отчество через пробел).
 */
$task36_1 = new Task36('Sergey', 'Pedchenko', 'Vladimirovich');
echo $task36_1 . '<br>';

/*
 * Task 37
 * Задача 37.1: Сделайте класс Date с публичными свойствами year (год), month (месяц) и day (день).
 * Задача 37.2: С помощью магии сделайте так, чтобы в классе могли появляться новые свойства,
 * без их прописывания в структуре класса, засетьте 2 таких свойства и выведите их на экран..
 * Задача 37.3: С помощью магии сделайте свойство weekDay,
 * которое будет возвращать день недели, соответствующий дате.
 * Задача 37.4: Сделайте возможность вызова 2 методов,
 * которые добавятся в наш класс со временем, но сейчас их нет или они не доступны.
 */
$task37_1 = new Task37();
$task37_1->year = 2020;
$task37_1->month = 2;
$task37_1->day = 4;

$task37_1->name = 'Sergey';
$task37_1->surname = 'Pedchenko';

echo $task37_1->name . '<br>';
echo $task37_1->surname . '<br>';

echo $task37_1->weekDay . '<br>';

$task37_1->method1();
$task37_1->method2();

/*
 * Task 42
 * Задача 42.1: Попрофилируйте свой код одни из способов,
 * рассмотренных на занятии (средствами PHP, XDebug, XHProf).
 * Задача 42.2: Посмотрите, какие узкие места есть в вашем коде, попробуйте оптимизировать их.
 */
$start = microtime(true);
$task42 = new Task36('Sergey', 'Pedchenko', 'Vladimirovich');
echo '<br>' . $task42 . '<br>';
$time = microtime(true) - $start;
echo $time . '<br>';

/*
 * Task 38
 * Задача 38.1: Создайте 3 разные кастомные исключения.
 * В первом смените сообщение на свое, второе - унаследуйте от третьего и позаботьтесь о том,
 * чтобы при выбросе второго исключения, выводился весь его стек трейс.
 * Задача 38.2: Выбросьте первое исключение, словите его и запишите логи об этом.
 * Задача 38.3: При выбрасывании второго исключения, словите его, запишите логи и пробросьте его дальше по цепочке.
 */
try {
    $directory = __DIR__ . '/config';
    if (!is_dir($directory)) {
        throw new FirstException('Directory `config` is not exists');
    }
} catch (FirstException $e) {
    error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/logs.log');
    echo $e->getMessage();
} catch (Exception $e) {
    echo $e->getMessage();
}

try {
    $config_file_path = __DIR__ . "/config.php";

    if (!file_exists($config_file_path)) {
        throw new SecondException("File not found.");
    }

    if (!is_writable($config_file_path)) {
        throw new ThirdException('File `config` is not writable');
    }
} catch (SecondException $e) {
    echo $e->getTraceAsString();
    error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/logs.log');
} catch (ThirdException $e) {
    echo $e->getMessage();
    error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/logs.log');
} catch (Exception $e) {
    echo $e->getMessage();
    error_log($e->getMessage() . PHP_EOL, 3, __DIR__ . '/logs.log');
}

/*
 * Task 41
 * Задача 41.1: Используя register_shutdown_function() напишите обработчик ошибок для вашего кода.
 * Проверьте, чтобы все работало как нужно и вы отлавливали все ошибки кода.
 * Задача 41.2: Сделайте в своем коде несколько разных типов ошибок, словите их и залогируйте.
 * Задача 41.3: Логи сложите либо средствами PHP в файл (более простой вариант),
 * либо через Monolog запишите в файл (более продвинутый вариант).
 * По желанию, можете использовать и другие способы, доступные в Monolog.
 */

new Task41();
//generate errors
include __DIR__ . '/test.php';
require 'null';
//all errors write to logs.log

// Create the logger
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new StreamHandler(__DIR__.'/my_app.log', Logger::DEBUG));
$logger->pushHandler(new FirePHPHandler());

// You can now use your logger
$logger->info('My logger is now ready');


/*
 * Task 43
 * Задача 43.1: Займитесь отладкой (подебажьте) вашей домашней работы с помощью XDebug.
 * Посмотрите, какие есть функции, что значат разные кнопки, попробуйте выполнить какой-то код в разные этапы дебага.
 * Задача 43.2: Сделайте минимум 5 скриншотов вашего дебага на разных его этапах.
 */
//http://joxi.ru/vAWQO8Ecq1BaLm
//http://joxi.ru/l2ZWxqztEwROXA
//http://joxi.ru/nAyWJ1ltjYaVEm
//http://joxi.ru/ZrJ7kKxsM9bRXm
//http://joxi.ru/bmoMJKyf9x7w7A
